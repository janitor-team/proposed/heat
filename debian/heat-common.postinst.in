#!/bin/sh

set -e

ETC=/etc/heat
CONF=/etc/heat/heat.conf

#PKGOS-INCLUDE#

create_heat_domain () {
        local ADMIN_USER ADMIN_PASS NO_PROTO AFTER_PORT WRITE_CRED_URL WRITE_CRED_PROTO
        ADMIN_USER=${1}
        ADMIN_PASS=${2}

        db_get heat/ksat-public-url
        KEYSTONE_PUBLIC_ENDPOINT=${RET}

        # Should we create a new keystone user?
        db_get heat/ksat-create-service-user
        if [ "${RET}" = "true" ] ; then
                # Set command line credentials
                export OS_USERNAME=${ADMIN_USER}
                db_get heat/ksat-admin-project-name
                export OS_TENANT_NAME=${RET}
                export OS_PROJECT_NAME=${RET}
                export OS_PASSWORD=${ADMIN_PASS}
                export OS_AUTH_URL=${KEYSTONE_PUBLIC_ENDPOINT}
                export OS_IDENTITY_API_VERSION=3
                export OS_PROJECT_DOMAIN_ID=default
                export OS_USER_DOMAIN_ID=default
                export OS_AUTH_TYPE=password

                db_get heat/heat_domain_name
                if [ -n "${RET}" ] ; then
                    HEAT_DOMAIN_NAME=${RET}
                fi

                echo "===> heat-common: Creating domain heat ..."
                openstack domain create --or-show --description "Stack projects and users" ${HEAT_DOMAIN_NAME}
        fi
}

create_heat_domain_admin () {
        local ADMIN_USER ADMIN_PASS NO_PROTO AFTER_PORT WRITE_CRED_URL WRITE_CRED_PROTO
        ADMIN_USER=${1}
        ADMIN_PASS=${2}

        db_get heat/ksat-public-url
        KEYSTONE_PUBLIC_ENDPOINT=${RET}

        # Should we create a new keystone user?
        db_get heat/ksat-create-service-user
        if [ "${RET}" = "true" ] ; then
                # Set command line credentials
                export OS_USERNAME=${ADMIN_USER}
                db_get heat/ksat-admin-project-name
                export OS_TENANT_NAME=${RET}
                export OS_PROJECT_NAME=${RET}
                export OS_PASSWORD=${ADMIN_PASS}
                export OS_AUTH_URL=${KEYSTONE_PUBLIC_ENDPOINT}
                export OS_IDENTITY_API_VERSION=3
                export OS_PROJECT_DOMAIN_ID=default
                export OS_USER_DOMAIN_ID=default
                export OS_AUTH_TYPE=password

                db_get heat/heat_domain_admin_username
                if [ -n "${RET}" ] ; then
                    HEAT_DOMAIN_ADMIN=${RET}
                fi
                db_get heat/heat_domain_admin_password
                if [ -n "${RET}" ] ; then
                    HEAT_DOMAIN_ADMIN_PASS=${RET}
                fi
                db_get heat/heat_domain_name
                if [ -n "${RET}" ] ; then
                    HEAT_DOMAIN_NAME=${RET}
                fi

                echo "===> heat-common: Creating user ${HEAT_DOMAIN_ADMIN} ..."
                openstack user create --or-show --domain ${HEAT_DOMAIN_NAME} --password ${HEAT_DOMAIN_ADMIN_PASS} --email root@localhost --enable ${HEAT_DOMAIN_ADMIN}

                echo "===> heat-common: Adding role domain admin to the user ${HEAT_DOMAIN_ADMIN} for domain ${HEAT_DOMAIN_NAME}"
                openstack role add --domain ${HEAT_DOMAIN_NAME} --user-domain ${HEAT_DOMAIN_NAME} --user ${HEAT_DOMAIN_ADMIN} admin
        fi
}

manage_trustee_config () {
        db_get heat/ksat-service-username
        pkgos_inifile set ${CONF} trustee username ${RET}

	db_get heat/ksat-service-password
        pkgos_inifile set ${CONF} trustee password ${RET}

        db_get heat/ksat-admin-url
        pkgos_inifile set ${CONF} trustee auth_url ${RET}
}

manage_clients_keystone_config () {
        db_get heat/ksat-public-url
        pkgos_inifile set ${CONF} clients_keystone auth_uri ${RET}
}

manage_domain_config () {
        db_get heat/heat_domain_admin_username
        pkgos_inifile set ${CONF} DEFAULT stack_domain_admin ${RET}

	db_get heat/heat_domain_admin_password
        pkgos_inifile set ${CONF} DEFAULT stack_domain_admin_password ${RET}

        db_get heat/heat_domain_name
        pkgos_inifile set ${CONF} DEFAULT stack_user_domain_name ${RET}
}

if [ "$1" = "configure" ] || [ "$1" = "reconfigure" ] ; then
	. /usr/share/debconf/confmodule
	. /usr/share/dbconfig-common/dpkg/postinst

	pkgos_var_user_group heat

	pkgos_write_new_conf heat heat.conf
	# We've switched to policy.d folder using yaml
	if [ -r /etc/heat/policy.json ] ; then
		mv /etc/heat/policy.json /etc/heat/disabled.policy.json.old
	fi

	db_get heat/configure_with_debconf
	if [ "${RET}" = "true" ] ; then
		manage_domain_config
	fi
	db_unregister heat/configure_with_debconf

	db_get heat/configure_db
	if [ "$RET" = "true" ] ; then
		pkgos_dbc_postinst ${CONF} database connection heat $@
	fi
	pkgos_rabbit_write_conf ${CONF} oslo_messaging_rabbit heat
	db_get heat/configure_ksat
	if [ "${RET}" = "true" ] ; then
		db_get heat/ksat-admin-password
		ADMIN_PASS=${RET}
		db_get heat/ksat-admin-username
		ADMIN_USER=${RET}
		manage_trustee_config
		manage_clients_keystone_config
		pkgos_write_admin_creds ${CONF} keystone_authtoken heat
		create_heat_domain ${ADMIN_USER} ${ADMIN_PASS}
		create_heat_domain_admin ${ADMIN_USER} ${ADMIN_PASS}
	fi


	# This is the equivalent of db-sync:
	db_get heat/configure_db
	if [ "$RET" = "true" ] ; then
		su heat -s /bin/sh -c "heat-manage db_sync"
	fi
	db_stop
fi

#DEBHELPER#
