Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: heat
Upstream-Contact: OpenStack Devel <openstack-dev@lists.openstack.org>
Source: https://github.com/openstack/heat

Files: *
Copyright: (c) 2013-2015, IBM Corp.
           (c) 2010-2016, OpenStack Foundation
           (c) 2017, Ericsson
           (c) 2013-2016, Hewlett-Packard Development Company, L.P.
           (c) 2014, Mirantis Inc.
           (c) 2013-2016, NTT DATA.
           (c) 2012-2016, Red Hat, Inc.
           (c) 2013, Unitedstack Inc.
           (c) 2010, US Gov. as represented by the Administrator of NASA
           (c) 2014-2015, Intel Corp.
           (c) 2013, eNovance
           (c) 2011, X.commerce, a business unit of eBay Inc.
           (c) 2016, Huawei Technologies India Pvt Ltd
License: Apache-2

Files: debian/*
Copyright: (c) 2012-2019, Thomas Goirand <zigo@debian.org>
           (c) 2016-2019, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.
